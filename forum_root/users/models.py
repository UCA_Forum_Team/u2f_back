import uuid
from django.db import models
from django.utils import timezone
from .managers import UserManager
from django_countries.fields import CountryField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

_

class User(AbstractBaseUser):
    user_id = models.UUIDField(verbose_name=_('User ID'), primary_key=True, default=uuid.uuid4, editable=False)
    
    email = models.EmailField(verbose_name=_('Email Adress'),
                              max_length=65,
                              unique=True,
                              db_index=True,
                              db_column='admin_email')

    nickname = models.CharField(verbose_name=_('Nickname'),
                                max_length=26,
                                db_index=True,
                                db_column='nickname')
    date_joined = models.DateField(verbose_name=_('Date Joined'), default=timezone.now)
    active = models.BooleanField(verbose_name=_('Is_Active'), default=True)
    staff = models.BooleanField(verbose_name=_('Is_Staff'), default=False)
    admin = models.BooleanField(verbose_name=_('Is_Admin'), default=False)
    student = models.BooleanField(verbose_name=_('Is_Student'), default=False)
    sla = models.BooleanField(verbose_name=_('Is_SLA'), default=False)
    teacher = models.BooleanField(verbose_name=_('Is_Teacher'), default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nickname']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
    
    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True
    
    def has_module_perms(self, app_label):
        return True

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_active(self):
        return self.active

    def __str__(self):
        return super().__str__(self.email)


class Student(models.Model):
    MALE = 'ML'
    FEMALE = 'FML'

    GENDER_CHOICES = [
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    ]

    CS = 'CS'
    COMMS = 'COMMS'

    MAJORS = [
        (CS, 'Computer Science'),
        (COMMS, 'Communications and Mass Media')
    ]

    NR = 'NR'
    KHR = 'KHR'

    CAMPUSES = [
        (NR, 'Naryn'),
        (KHR, 'Khorog')
    ]

    student = models.OneToOneField(User, verbose_name=_('Student'), null = True, related_name='Students', on_delete=models.PROTECT)

    nickname = models.CharField(verbose_name=_('Student Nickname'),
                                max_length=26,
                                db_index=True,
                                db_column='nickname')

    birth_date = models.DateField(verbose_name=_('birth_date'),
                                  db_column='birth_date')

    gender = models.CharField(verbose_name=_('gender'),
                              max_length=3, 
                              choices=GENDER_CHOICES,
                              db_column='gender')

    major = models.CharField(verbose_name=_('Major'),
                             max_length=5,
                             choices=MAJORS,
                             db_column='major')

    campus = models.CharField(verbose_name=_('Campus'),
                              max_length=3,
                              choices=CAMPUSES,
                              db_column='campus')

    photo = models.ImageField(verbose_name=_('Profile Photo'),
                              upload_to='student_profile/',
                              height_field=500,
                              width_field=500,
                              max_length=65)

    country = CountryField(verbose_name=_('Country From'), db_column='country')
    is_ucasa = models.BooleanField(verbose_name=_('UCASA'), default=False)
    is_moderator = models.BooleanField(verbose_name=_('Moderator'), default=False)
    is_owner = models.BooleanField(verbose_name=_('Service Owner'), default=False)
    is_club_creator = models.BooleanField(verbose_name=_('Club Creator'), default=False)

    phone_number = models.IntegerField(verbose_name=_('phone_number'),
                                       blank=True,
                                       db_column='phone_number')

    has_card = models.BooleanField(verbose_name=_('Has Card'),
                                   default=False,
                                   db_column='has_card')

    student.is_banned = False

    def __str__(self):
        return super().__str__(self.student)



class Teacher(models.Model):
    MALE = 'ML'
    FEMALE = 'FML'

    GENDER_CHOICES = [
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    ]

    SCIENCE = 'SCN'
    ART = 'ART'

    DEPARTMENTS = [
        (SCIENCE, 'Science'),
        (ART, 'Art')
    ]

    NR = 'NR'
    KHR = 'KHR'

    CAMPUSES = [
        (NR, 'Naryn'),
        (KHR, 'Khorog')
    ]

    teacher = models.OneToOneField(User, verbose_name=_('Teacher'), null = True, related_name='teachers', on_delete=models.PROTECT)

    nickname = models.CharField(verbose_name=_('Teacher Nickname'),
                                max_length=26,
                                db_index=True,
                                db_column='nickname')

    birth_date = models.DateField(verbose_name=_('birth_date'),
                                  db_column='birth_date')

    gender = models.CharField(verbose_name=_('gender'),
                              max_length=3, 
                              choices=GENDER_CHOICES,
                              db_column='gender')

    department = models.CharField(verbose_name=_('department'),
                             max_length=3,
                             choices=DEPARTMENTS,
                             db_column='department')

    campus = models.CharField(verbose_name=_('Campus'),
                              max_length=3,
                              choices=CAMPUSES,
                              db_column='campus')

    photo = models.ImageField(verbose_name=_('Profile Photo'),
                              upload_to='teacher_profile/',
                              height_field=500,
                              width_field=500,
                              max_length=65)

    phone_number = models.IntegerField(verbose_name=_('phone_number'),
                                       blank=True,
                                       db_column='phone_number')

    has_card = models.BooleanField(verbose_name=_('Has Card'),
                                   default=False,
                                   blank=True,
                                   db_column='has_card')

    country = CountryField(verbose_name=_('Country From'), db_column='country')
    is_club_creator = models.BooleanField(verbose_name=_('Club Creator'), default=False)

    class Meta:
        verbose_name = _('Teacher')
        verbose_name_plural = _('Teachers')

    def __str__(self):
        return super().__str__(teacher)



class StudentAdvisor(models.Model):
    MALE = 'ML'
    FEMALE = 'FML'

    GENDER_CHOICES = [
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    ]

    NR = 'NR'
    KHR = 'KHR'

    CAMPUSES = [
        (NR, 'Naryn'),
        (KHR, 'Khorog')
    ]

    advisor = models.OneToOneField(User, verbose_name=_('Studnet Advisor'), null = True, related_name='StudnetAdvisor', on_delete=models.PROTECT)

    nickname = models.CharField(verbose_name=_('Advisor Nickname'),
                                max_length=26,
                                db_index=True,
                                db_column='nickname')

    birth_date = models.DateField(verbose_name=_('birth_date'),
                                  db_column='birth_date')

    gender = models.CharField(verbose_name=_('gender'),
                              max_length=3, 
                              choices=GENDER_CHOICES,
                              db_column='gender')

    campus = models.CharField(verbose_name=_('Campus'),
                              max_length=3,
                              choices=CAMPUSES,
                              db_column='campus')

    photo = models.ImageField(verbose_name=_('Profile Photo'),
                              upload_to='studentAdvisor_profile/',
                              height_field=500,
                              width_field=500,
                              max_length=65)

    phone_number = models.IntegerField(verbose_name=_('phone_number'),
                                       blank=True,
                                       db_column='phone_number')

    has_card = models.BooleanField(verbose_name=_('Has Card'),
                                   default=False,
                                   blank=True,
                                   db_column='has_card')

    country = CountryField(verbose_name=_('Country From'), db_column='country')
    is_club_creator = models.BooleanField(verbose_name=_('Club Creator'), default=False)

    class Meta:
        verbose_name = _('StudentAdvisor')
        verbose_name_plural = _('StudentAdvisors')

    def __str__(self):
        return super().__str__(advisor)